if [ -n "$(command -v bat)" ]; then
  export MANPAGER="sh -c 'col -bx | bat -l man -p'"
fi

# Sources
export PATH=$PATH:~/.local/bin:~/bin
export SPACESHIP_EXIT_CODE_SHOW=true
